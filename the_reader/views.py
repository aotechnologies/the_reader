from django.views.generic import TemplateView
from feeds.models import FeedSource


# Views

class HomeView(TemplateView):
    template_name = 'index.html'

    title = 'Accueil'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = self.title
        context['feed_sources'] = FeedSource.objects.all()

        return context
