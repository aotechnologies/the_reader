$().ready(() => {
    $('[data-toggle="popover"]').popover({
        trigger: 'hover',
        placement: 'bottom',
        html: true,
        delay: {show: 500, hide: 100},
    });
});