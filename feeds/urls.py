from django.urls import path
from . import views

app_name = 'feeds'

urlpatterns = [
    path('category/<slug:slug>/', views.CategoryDetailView.as_view(), name='category_detail'),
    path('fetch/', views.get_feeds, name='fetch')
]
