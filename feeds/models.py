from django.db import models
from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin
from django.utils.text import slugify

# Create your models here.
User = get_user_model()


class Category(LoginRequiredMixin, models.Model):
    name = models.CharField(max_length=100, unique=True)
    created_at = models.DateTimeField(auto_now_add=True)
    slug = models.SlugField(editable=False, blank=True, default='')

    def __str__(self):
        return self.name

    def save(self, **kwargs):
        self.slug = slugify(self.name)
        super().save(**kwargs)

    class Meta:
        ordering = ['name']


class FeedSource(LoginRequiredMixin, models.Model):
    name = models.CharField(max_length=256, unique=True)
    url = models.URLField(unique=True)
    description = models.TextField(blank=True, null=True, default='')
    user = models.ForeignKey(User, related_name='sources', blank=True, null=True, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    categories = models.ManyToManyField(Category, related_name='sources')

    def __str__(self):
        return self.name

    def get_articles(self):
        return self.articles.all().order_by('-published_at')[:8]

    class Meta:
        ordering = ['name']


class Feed(LoginRequiredMixin, models.Model):
    name = models.CharField(max_length=256, blank=True, default='')
    url = models.URLField(unique=True)
    feed_source = models.ForeignKey(FeedSource, related_name='feeds', on_delete=models.CASCADE)
    category = models.ForeignKey(Category, related_name='feeds', on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    last_updated = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.name = self.category.name + '_' + self.feed_source.name
        super().save(*args, **kwargs)


class Article(models.Model):
    title = models.CharField(max_length=256)
    text = models.TextField()
    link = models.URLField(blank=True, null=True, default='')
    image_url = models.URLField(blank=True, null=True, default='')
    created_at = models.DateTimeField(auto_now_add=True)
    published_at = models.DateTimeField()
    feed_source = models.ForeignKey(FeedSource, related_name='articles', on_delete=models.CASCADE)
    category = models.ForeignKey(Category, related_name='articles', on_delete=models.CASCADE)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['published_at']
