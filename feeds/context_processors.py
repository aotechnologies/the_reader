from .models import Category


def categories(request):
    # Get categories list
    cats = Category.objects.all().order_by('name')

    return {
        'categories': cats
    }
