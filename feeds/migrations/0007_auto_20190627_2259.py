# Generated by Django 2.2.1 on 2019-06-27 22:59

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('feeds', '0006_auto_20190627_2251'),
    ]

    operations = [
        migrations.RenameField(
            model_name='feedsource',
            old_name='feed_sources',
            new_name='categories',
        ),
    ]
