from django.contrib import admin
from .models import FeedSource, Category, Feed


# Register your models here.

@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    exclude = ['created_at']
    date_hierarchy = 'created_at'
    list_display = ['name', 'slug']
    search_fields = ['name']


@admin.register(FeedSource)
class FeedSourceAdmin(admin.ModelAdmin):
    fields = ['name', 'url', 'categories', 'description']
    list_display = ['name', 'url', 'description', 'added_by', 'related_categories']
    filter_vertical = ['categories']
    search_fields = ['name']

    def added_by(self, obj):
        username = ''
        if obj.user:
            username = obj.user.username

        return username

    def related_categories(self, obj):
        categories = obj.categories.all()

        return [category.name for category in categories]


@admin.register(Feed)
class FeedAdmin(admin.ModelAdmin):
    list_display = ['name', 'url']
    search_fields = ['name']
    fieldsets = [
        (
            None, {
                'fields': ['category', 'feed_source']
            }
        ),
        (
            None, {
                'fields': ['url']
            }
        ),
    ]
