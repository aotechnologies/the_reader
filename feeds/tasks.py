from datetime import datetime

import feedparser
from dateutil import parser

from feeds.models import FeedSource, Article

for fs in FeedSource.objects.all():
    # Fetch all feeds
    for feed in fs.feeds.all():
        rss_feed = feedparser.parse(feed.url)

        # Save articles
        for entry in rss_feed.entries:
            entry_date = parser.parse(entry.published)

            if not feed.last_updated or (entry_date >= feed.last_updated):
                try:
                    article = Article(title=entry.title, link=entry.link, text=entry.summary,
                                      published_at=parser.parse(entry.published),
                                      image_url=entry.links[1]['href'], feed_source=fs, category=feed.category)

                    article.save()
                except AttributeError as error:
                    pass

        feed.last_updated = datetime.now()
        feed.save()

print('OK')
