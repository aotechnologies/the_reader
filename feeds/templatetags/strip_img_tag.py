from django import template
from django.template.defaultfilters import stringfilter
import re

register = template.Library()


@register.filter
@stringfilter
def strip_img(value):
    pattern_object = re.compile(r'<img.*?/>')
    return pattern_object.sub('', value)

