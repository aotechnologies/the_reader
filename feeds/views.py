from django.views.generic import DetailView
from django.http import HttpResponseRedirect

from .models import FeedSource, Category, Article

from datetime import datetime

import feedparser
from dateutil import parser


# Create your views here.

def get_feeds(req):
    for fs in FeedSource.objects.all():
        for feed in fs.feeds.all():
            rss_feed = feedparser.parse(feed.url)
            # Save articles
            for entry in rss_feed.entries:
                entry_date = parser.parse(entry.published)

                if not feed.last_updated or (entry_date >= feed.last_updated):
                    try:
                        article = Article(title=entry.title, link=entry.link, text=entry.summary,
                                          published_at=entry_date,
                                          image_url=entry.links[1]['href'],
                                          feed_source=fs,
                                          category=feed.category)
                        article.save()
                    except (AttributeError, IndexError) as error:
                        print(error)
                        pass

            feed.last_updated = datetime.now()
            feed.save()

    return HttpResponseRedirect('/')


class CategoryDetailView(DetailView):
    model = Category
    context_object_name = 'category'

    def get_context_data(self, **kwargs):
        category = self.get_object()
        context = super().get_context_data(**kwargs)
        feeds_sources = FeedSource.objects.filter(categories__name__contains=category.name)

        context['feeds_sources'] = feeds_sources
        context['title'] = category.name

        return context
